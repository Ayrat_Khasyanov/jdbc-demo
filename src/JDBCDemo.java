import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class JDBCDemo {
        public static void main(String args[]) { 
            try { 
                runTest(); 
            } catch (SQLException ex) {
                for (Throwable t : ex) 
                    t.printStackTrace(); 
            } catch (IOException ex) {
                ex.printStackTrace(); 
            } 
        }

    private static void runTest() throws SQLException, IOException {
        Connection conn = getConnection();
        try {
            Statement stat = conn.createStatement();
            stat.executeUpdate("CREATE TABLE test (col CHAR(7))");
            stat.executeUpdate("INSERT INTO test VALUES ('Waw')");
            ResultSet result = stat.executeQuery("SELECT * FROM test");
            if (result.next()) System.out.println(result.getString(1));
            result.close();
            stat.executeUpdate("DROP TABLE test");
        } finally { conn.close();}
    }
    public static Connection getConnection() throws SQLException, IOException {
        Properties props = new Properties();
        FileInputStream in = new FileInputStream("postgres.properties");
        props.load(in);
        in.close();
        String drivers = props.getProperty("jdbc.drivers");
        if (drivers != null) System.setProperty("jdbc.drivers", drivers);
        String url = props.getProperty("jdbc.url");
        String username = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");
        return DriverManager.getConnection(url, username, password);
    }
}
