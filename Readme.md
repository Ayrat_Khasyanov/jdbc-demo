
To pull the image from the repository:
docker pull postgres

To run the image:
docker run --name some-postgres -p 5432:5432 -e POSTGRES_PASSWORD=password -e POSTGRES_USER=user -d postgres

Before executing the code for postgres:
Create database 'informatics' 